// fetch() - used to perform CRUD operations in a given URL
    //     - accepts 2 arguments/parameter (url and options/callback function)
    // the option parameter is only used when the dev needs the requestbody from the user


// GET the post data
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then(data => showPost(data))


// ADD Post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();

    // "option"
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method : "POST",
        body : JSON.stringify({
            title:document.querySelector('#txt-title').value,
            body:document.querySelector('#txt-body').value,
            userId:1
        }),
        headers:{"Content-Type": "application/json; charset =UTF 8"}
    })
    .then(response => response.json())
    .then(data => {
        // showPost([data]) - to show the recently created post ONLY
        alert("Post Created Successfully")

        // to clear the text in the input fields upon creating a post
        // showPost(data)
        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null;

    })
})


// SHOW Post
const showPost = (posts) =>{
    let postEntries = [];
    
	posts.forEach((post) => {
        postEntries += `
        <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        
        <p id="post-body-${post.id}">${post.body}</p>
        
        <button onclick="editPost('${post.id}')">Edit</button>
        
        <button onclick="deletePost('${post.id}')">Delete</button>
        
        </div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}


// EDIT Post

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

    // removeAttribute -  remove the attribute from the element; it receives a string arguement that servers to be the attribute of the element
    document.querySelector("#btn-submit-update").removeAttribute('disabled')
}

//UPDATE post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault()
    fetch ( 'https://jsonplaceholder.typicode.com/posts', {

        method : 'PUT',
        body : JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId:1
        }),
        headers:{"Content-Type" : "application/json; charset=UTF-8"}
    })
    .then(response => response.json())
    .then(data => {
        console.log(data)
        alert('Post Successfully Updated')

        // clears the input field
        document.querySelector("#txt-edit-id").value = null;
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;

        // setattribute - sets the attribute of the element; accepts two arguments
            //string - the attribute to be set
            // boolean - true/false
        document.querySelector("#btn-submit-update").setAttribute('disabled', true)
    })
}) 



// DELETE Post
/* const deletePost = (id) => {
    fetch('https://jsonplaceholder.typicode.com/posts/', {
        method: 'DELETE'
    })
} */

const deletePost = (id) => {
    document.querySelector(`#post-${id}`).remove();
}